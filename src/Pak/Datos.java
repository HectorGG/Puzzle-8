package Pak;


import java.util.ArrayList;

import Algoritmos.AlgoritmoBusqueda;
import Algoritmos.BusquedaAnchura;
import Algoritmos.BusquedaProfundidad;
import Algoritmos.BusquedaProfundidadIterativa;
import Algoritmos.Estrella;

public class Datos {
	private ArrayList<AlgoritmoBusqueda> TiposAlgoritmos=new ArrayList<AlgoritmoBusqueda>();
	private Nodo Ini,Final;
	private String opciones;
	protected long time_start, time_end;
	
	
	public Datos(Nodo a, Nodo b,String opc){
		Ini=a;
		Final=b;
		opciones=opc;
	}
	
	public void  AlgoritmosBusqueda(){
		switch(opciones){
		case "Anchura": 
				AlgoritmoBusqueda aux=new BusquedaAnchura(Ini,Final);
				time_start = System.currentTimeMillis();
				aux.Busqueda();
				time_end = System.currentTimeMillis();
				aux.setTime(time_end-time_start);
                TiposAlgoritmos.add(aux);
				break;
		case "Profundidad": 
				AlgoritmoBusqueda aux2=new BusquedaProfundidad(Ini,Final,25);
				time_start = System.currentTimeMillis();
				aux2.Busqueda();
				time_end = System.currentTimeMillis();
				aux2.setTime(time_end-time_start);
                TiposAlgoritmos.add(aux2); 
				break;
		case "Iterativa": 
				AlgoritmoBusqueda aux3= new BusquedaProfundidadIterativa(Ini,Final,25);
				time_start = System.currentTimeMillis();
				aux3.Busqueda();
				time_end = System.currentTimeMillis();
				aux3.setTime(time_end-time_start);
                TiposAlgoritmos.add(aux3); 
				break;
		case "Estrella": 
				AlgoritmoBusqueda aux4= new Estrella(Ini,Final);
				time_start = System.currentTimeMillis();
				aux4.Busqueda();
				time_end = System.currentTimeMillis();
				aux4.setTime(time_end-time_start);
				TiposAlgoritmos.add(aux4); 
				break;
		}
	}
	
	public String PresentacionDatos(int opc){
		String aux="\n";
		for(int i=0; i<TiposAlgoritmos.size();i++ ){
		switch(opc){
			case 0: if(TiposAlgoritmos.get(i).getEncontrado()){
						TiposAlgoritmos.get(i).GetRecorrido();
						aux=aux+TiposAlgoritmos.get(i).GetNombre()+";"+Ini+";";
						TiposAlgoritmos.get(i).getPasosRecorridos();
						aux=aux+TiposAlgoritmos.get(i).toString(0);
					}
					else
						{/*aux=aux+TiposAlgoritmos.get(i).GetNombre();
						 aux=aux+";No_encontrado";*/}
					break;
			case 1:if(TiposAlgoritmos.get(i).getEncontrado()){
						aux=aux+TiposAlgoritmos.get(i).GetNombre();
						aux=aux+TiposAlgoritmos.get(i).GetRecorrido();
						aux=aux+TiposAlgoritmos.get(i).getPasosRecorridos();
						aux=aux+TiposAlgoritmos.get(i).toString(2);
					}else
						{aux=aux+TiposAlgoritmos.get(i).GetNombre();
						 aux=aux+";No_encontrado";}
					break;
			}
		}
	return aux;
	}
					
}
