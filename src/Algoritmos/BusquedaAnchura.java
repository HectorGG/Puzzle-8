package Algoritmos;
import java.util.ArrayList;

import Pak.Nodo;

public class BusquedaAnchura extends AlgoritmoBusqueda{
	private String Nombre;
	int numestados;
	
	
	
	public BusquedaAnchura(Nodo ini, Nodo f){
		nodo=ini;
		EstFinal=f;
		Nombre="Busqueda Altura";
		numestados=1;
		encontrado=false;
	}
	
	public void Busqueda(){
		//Inicializacion//
		Abierto().add(nodo);
		resultado=Abierto().get(0);
		
		while(Abierto().size()>0 && !esIgual(resultado,EstFinal) && resultado.getProfundidad()<18){
			Cerrado().add(resultado);
			Abierto().remove(0);
			//Generacion y tratamiento de la desendencia//
			this.insertarHijos(resultado.generarHijo());
			this.repite(Hijos(), Abierto(), Cerrado());
			this.insertarAbierto();
			//Cojo mi nuevo nodo de la tabla de abierto//
			resultado=Abierto().get(0);
			this.Memoria();
			Hijos().clear();
		}
		if(esIgual(resultado,EstFinal)){encontrado=true;}
		else{encontrado=false;}
		
	}
		
	
	public void repite(ArrayList<Nodo> Hijos , ArrayList<Nodo> Abierto , ArrayList<Nodo> Cerrado ){
		int i=0,j=0,k=0;
		boolean encontrado=false;
		
		while(i<Hijos().size()){
			j=0;
			encontrado=false;
			while(j<Abierto().size() && !encontrado){
				if(esIgual(Hijos().get(i),Abierto().get(j))){
					Hijos().remove(i);
					encontrado=true;
					//i--;
				}
				j++;
			}
			k=0;
			while(k<Cerrado().size()&& !encontrado){
				if(esIgual(Hijos().get(i),Cerrado().get(k))){
					Hijos().remove(i);
					encontrado=true;
					//i--;
				}
				k++;
			}
			i++;
			
		}
		
	}
	
	public String GetNombre() {return Nombre;}
	
	
}
