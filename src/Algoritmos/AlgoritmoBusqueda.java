package Algoritmos;

import java.util.ArrayList;

import Pak.Nodo;

public abstract class AlgoritmoBusqueda {
	private ArrayList<Nodo> Abierto = new ArrayList<Nodo>();
	private ArrayList<Nodo> Cerrado = new ArrayList<Nodo>();
	private ArrayList<Nodo> Hijos = new ArrayList<Nodo>();	
	protected Nodo EstFinal;
	protected Nodo nodo;
	protected Nodo resultado;
	protected int PasosRecorridos;
	protected int NAbiertos;
	protected int NCerrados;
	protected int Memoria;
	protected int EstVisitados;
	protected boolean encontrado;
	private long time;
	
	
	public abstract void  Busqueda();
	protected ArrayList<Nodo> Cerrado(){return Cerrado;}
	protected ArrayList<Nodo> Abierto(){return Abierto;}
	protected ArrayList<Nodo> Hijos(){return Hijos;}
	
	public int getnumEstados(){return EstVisitados;}
	public int getNa(){return NAbiertos;}
	public int getNC(){return NCerrados;}


    public abstract void repite(ArrayList<Nodo> Hijos , ArrayList<Nodo> Abierto , ArrayList<Nodo> Cerrado );
	public int getPasosRecorridos(){return PasosRecorridos;}
	public int getMemoria(){return Abierto.size()+Cerrado.size();}  //Memoria total opcional()
	public int getEstVisitados(){return EstVisitados;}
	public boolean getEncontrado(){return encontrado;}
	public void setTime(long t){time=t;}
	
	
	protected void insertarAbierto(){
		for(int i=0; i<Hijos.size();i++)
			Abierto.add(Hijos.get(i));
	}
	protected void insertarHijos(ArrayList<Nodo> h){
		for(int i=0; i<h.size();i++){
			Hijos.add(h.get(i));
			EstVisitados++;
		}
	}
	
	public boolean esIgual(Nodo h , Nodo id){
		boolean encontrado=true;
		int i=0, j=0;
		while(i<3 && encontrado){
			j=0;
			while(j<3 && encontrado){
				if(h.getestados(i,j)!= id.getestados(i,j))
					encontrado=false;
				j++;
			}
			i++;
		}
		
		return encontrado;
	}
	
	public  String toString(int nul){  
		String aux="";
		switch(nul){
		case 0: aux=aux+PasosRecorridos+";"+this.getnumEstados()+";"+this.getNa()+";"+this.getNC()+";"+time; break;
		case 1:
				for(int i=0; i<3; i++){
						for(int j=0;j<3;j++){
							aux=aux+nodo.getestados(i,j);
						}
				}
				break;
		case 2:	aux=aux+"\nPasos:"+PasosRecorridos+"\n"+"Estados Visitados:"+this.getnumEstados();
				aux=aux+"\nEstados Abiertos:"+this.getNa()+"\nEstados Cerrados"+this.getNC()+"\n";
		}
		return aux;
	}
	
	public String GetRecorrido(){ //Debuelve el recorrido de nodos que a seguido
		String aux= "";
		Nodo Naux=resultado;
		
		while(Naux.getPadre()!=null){
			aux=aux+Naux.toString(1);
			Naux=Naux.getPadre();
			PasosRecorridos++;
		}
		aux=aux+Naux.toString(1);
		return aux;
		
	}

	public void Memoria() {
		if(NAbiertos<Abierto.size())
			NAbiertos=Abierto.size();
		
		if(NCerrados<Cerrado.size())
			NCerrados=Cerrado.size();
	}

	
	public abstract String GetNombre();

}
