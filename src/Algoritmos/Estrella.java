package Algoritmos;

import java.util.ArrayList;

import Pak.Nodo;

public class Estrella extends AlgoritmoBusqueda {
	private String Nombre;
	int numestados;
	
	public Estrella(Nodo ini,Nodo f){
		Nombre="Estrella";
		nodo=ini;
		EstFinal=f;
		nodo.coste(EstFinal);
	}
	
	public void Busqueda(){
		//Inicializacion//
				Abierto().add(nodo);
				resultado=Abierto().get(0);
				
				while(Abierto().size()>0 && !esIgual(resultado,EstFinal)){
					Cerrado().add(resultado);
					Abierto().remove(0);
					//Generacion y tratamiento de la desendencia//
					this.insertarHijos(resultado.generarHijo());
					this.repite(Hijos(), Abierto(), Cerrado());
					for(int i=0;i<Hijos().size();i++)
						Hijos().get(i).coste(EstFinal);
					this.insertarAbierto();
					this.heuristico();
					//Cojo mi nuevo nodo de la tabla de abierto//
					if(Abierto().size()>0)
						resultado=Abierto().get(0);
					this.Memoria();
					Hijos().clear();
				}
				if(esIgual(resultado,EstFinal)){encontrado=true;}
				else{encontrado=false;}
				
	}
	
	public  void repite(ArrayList<Nodo> Hijos , ArrayList<Nodo> Abierto , ArrayList<Nodo> Cerrado ){
		int i=0,j=0,k=0;
		boolean encontrado=false;
		while(i<Hijos().size()){
			j=0;
			encontrado=false;
			while(j<Abierto().size() && !encontrado){
				if(esIgual(Hijos().get(i),Abierto().get(j))){
					/*if(Hijos().get(i).getProfundidad()< Abierto().get(j).getProfundidad())
						Abierto().set(j, Hijos().get(i));*/
					Hijos().remove(i);
					encontrado=true;
					i--;
				}
				j++;
			}
			while(k<Cerrado().size()&& !encontrado){
				if(esIgual(Hijos().get(i),Cerrado().get(k))){
					Hijos().remove(i);
					encontrado=true;
					i--;
				}
				k++;
			}
			i++;
			
		}
		
	}
	
	public void heuristico(){
		Nodo auxiliar;
		for(int i=0; i<Abierto().size(); i++){
			for(int j=0; j<Abierto().size()-1;j++){
			if(Abierto().get(j).getcoste()>Abierto().get(j+1).getcoste()){
				auxiliar=Abierto().get(j);
				Abierto().set(j, Abierto().get(j+1));
				Abierto().set(j+1,auxiliar);
			}
		/*	else if( Abierto().get(j).getProfundidad()>Abierto().get(j+1).getProfundidad()){
		    	auxiliar=Abierto().get(j);
				Abierto().set(j, Abierto().get(j+1));
				Abierto().set(j+1,auxiliar);
			}*/
		}
	}
}
	public String GetNombre() {return Nombre;}
	
}
